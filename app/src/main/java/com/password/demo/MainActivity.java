package com.password.demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.lnyp.pswkeyboard.widget.PopEnterPassword;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.tvBtn).setOnClickListener(view -> new PopEnterPassword(MainActivity.this).show(view));
    }
}