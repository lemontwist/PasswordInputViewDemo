package com.lnyp.pswkeyboard.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;

import com.lnyp.pswkeyboard.OnPasswordInputFinish;
import com.lnyp.pswkeyboard.R;


/**
 * 输入支付密码
 *
 * @author lining
 */
public class PopEnterPassword extends PopupWindow implements View.OnClickListener {

    private PasswordView pwdView;

    private View mMenuView;

    private Activity mContext;
    private OnPasswordInputFinish mPass;

    public PopEnterPassword(final Activity context) {

        super(context);

        this.mContext = context;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mMenuView = inflater.inflate(R.layout.pop_enter_password, null);

        pwdView = (PasswordView) mMenuView.findViewById(R.id.pwd_view);

        //添加密码输入完成的响应
//        pwdView.setOnFinishInput(new OnPasswordInputFinish() {
//            @Override
//            public void inputFinish(String password) {
//
//                dismiss();
//
//                Toast.makeText(mContext, "支付成功，密码为：" + password, Toast.LENGTH_SHORT).show();
//            }
//        });


        // 监听X关闭按钮
        pwdView.getImgCancel().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        // 监听键盘上方的返回
//        pwdView.getVirtualKeyboardView().getLayoutBack().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });

        // 设置SelectPicPopupWindow的View
        this.setContentView(mMenuView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.MATCH_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.pop_add_ainm);
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0x66000000);
        // 设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);

        initNoticeDialog();

    }

    private void initNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(false);
        View view = View.inflate(mContext, R.layout.layout_pw_notice_dg, null);
        builder.setView(view);
        noticeDialog = builder.create();
        view.findViewById(R.id.again_btn).setOnClickListener(this);
        view.findViewById(R.id.forget_pw_btn).setOnClickListener(this);
    }

    public void setOnFinishInput(final OnPasswordInputFinish pass) {
        mPass = pass;
        if (null != mPass) {
            pwdView.setOnFinishInput(mPass);
        }
    }

    //密码错误提示
    public void showNotice() {
        noticeDialog.show();
    }

    private AlertDialog noticeDialog;

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.forget_pw_btn) {
            noticeDialog.dismiss();
            mPass.onPasswordForget();
        } else if (view.getId() == R.id.again_btn) {
            noticeDialog.dismiss();
            //show();
            mPass.onInputAgain();
        }
    }

    public void clear() {
        pwdView.clear();
    }

    public void show(View view) {
        clear();
        showAtLocation(view,
                Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    public void show(View view, String takeNum) {
        show(view);
        pwdView.textAmount.setText(takeNum);
    }

    public void setPoundage(String s) {
        pwdView.poundage_tv.setText(s);
    }
}
